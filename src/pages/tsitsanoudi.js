import React from 'react'
import SpeakerLayout from '../components/speaker_layout'
import Layout from '../components/layout'
import { withIntl } from '../i18n';


const Tsitsanoudi = () => (
  <Layout>
    <SpeakerLayout speaker="tsitsanoudi" />
  </Layout>
)


export default withIntl(Tsitsanoudi)
