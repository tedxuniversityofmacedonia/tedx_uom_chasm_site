import React from 'react'
import SpeakerLayout from '../components/speaker_layout'
import Layout from '../components/layout'
import { withIntl } from '../i18n';


const Legaki = () => (
  <Layout>
    <SpeakerLayout speaker="legaki" />
  </Layout>
)


export default withIntl(Legaki)
