import React from 'react'
import SpeakerLayout from '../components/speaker_layout'
import Layout from '../components/layout'
import { withIntl } from '../i18n';


const Psychouli = () => (
  <Layout>
    <SpeakerLayout speaker="psychouli" />
  </Layout>
)


export default withIntl(Psychouli)