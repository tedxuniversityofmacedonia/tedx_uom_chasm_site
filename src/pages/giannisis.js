import React from 'react'
import SpeakerLayout from '../components/speaker_layout'
import Layout from '../components/layout'
import { withIntl } from '../i18n';


const Giannisis = () => (
  <Layout>
    <SpeakerLayout speaker="giannisis" />
  </Layout>
)


export default withIntl(Giannisis)
