import React from 'react'
import SpeakerLayout from '../components/speaker_layout'
import Layout from '../components/layout'
import { withIntl } from '../i18n';


const Levitikos = () => (
  <Layout>
    <SpeakerLayout speaker="levitikos" />
  </Layout>
)


export default withIntl(Levitikos)
