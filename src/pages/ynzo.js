import React from 'react'
import SpeakerLayout from '../components/speaker_layout'
import Layout from '../components/layout'
import { withIntl } from '../i18n';


const Ynzo = () => (
  <Layout>
    <SpeakerLayout speaker="ynzo" />
  </Layout>
)


export default withIntl(Ynzo)
