import React from 'react'
import SpeakerLayout from '../components/speaker_layout'
import Layout from '../components/layout'
import { withIntl } from '../i18n';


const Nazlidis = () => (
  <Layout>
    <SpeakerLayout speaker="nazlidis" />
  </Layout>
)


export default withIntl(Nazlidis)
