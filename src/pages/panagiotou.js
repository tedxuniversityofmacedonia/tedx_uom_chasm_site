import React from 'react'
import SpeakerLayout from '../components/speaker_layout'
import Layout from '../components/layout'
import { withIntl } from '../i18n';


const Panagiotou = () => (
  <Layout>
    <SpeakerLayout speaker="panagiotou" />
  </Layout>
)


export default withIntl(Panagiotou)
