import React from 'react'
import { injectIntl } from 'react-intl'
import Helmet from 'react-helmet'
import Header from './header'
import Footer from './footer'

import { Grommet, Box } from 'grommet'
import favicon from '../assets/favicon.ico'
import siteScreenshot from '../assets/site_screenshot.png'

import '../scss/main.scss'

const Layout = ({ children, data, intl }) => (
  <Grommet>
    <Helmet
      title="TEDxUniversityofMacedonia 2018 Chasm"
      meta={[
        {
          name: 'description',
          content:
            'Chasm ή αλλιώς χάσμα. Το φαινόμενο που δεν πέρασε ποτέ απαρατήρητο. Το κομμάτι της καθημερινότητας σου, που σε αγγίζει και σε προβληματίζει.',
        },
        {
          property: 'og:image',
          content: siteScreenshot,
        },
        {
          property: 'og:title',
          content: 'TEDxUniversityofMacedonia 2018 Chasm',
        },
        {
          property: 'og:url',
          content: 'https://www.tedxuniversityofmacedonia.com',
        },
        {
          property: 'og:description',
          content:
            'Chasm ή αλλιώς χάσμα. Το φαινόμενο που δεν πέρασε ποτέ απαρατήρητο. Το κομμάτι της καθημερινότητας σου, που σε αγγίζει και σε προβληματίζει.',
        },
        {
          property: 'og:type',
          content: 'website',
        },
      ]}
    >
      <link rel="shortcut icon" type="image/png" href={favicon} sizes="16x16" />
    </Helmet>

    <Header siteTitle={intl.formatMessage({ id: 'title' })} />
    <Box align="center">{children}</Box>
    <Footer />
  </Grommet>
)

export default injectIntl(Layout)
