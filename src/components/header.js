import React, { Component } from 'react'
import {Box} from 'grommet/'
import Toolbar from './toolbar'
import SideDrawer from './SideDrawer'
import Backdrop from './Backdrop'

class SiteHeader extends Component {
  state = {
    sideDrawerOpen: false,
  }

  drawerToggleClickHandler = () => {
    this.setState(prevState => {
      return { sideDrawerOpen: !prevState.sideDrawerOpen }
    })
  }

  backdropClickHandler = () => {
    this.setState({ sideDrawerOpen: false })
  }
  render() {
    let backdrop

    if (this.state.sideDrawerOpen) {
      backdrop = <Backdrop click={this.backdropClickHandler} />
    }
    return (
      <Box pad="none" style={{marginBottom: '68px'}}>
        <Toolbar drawerClickHandler={this.drawerToggleClickHandler} />
        <SideDrawer show={this.state.sideDrawerOpen} />
        {backdrop}
      </Box>
    )
  }
}

export default SiteHeader
