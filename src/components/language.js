import React, { Component } from 'react'
import PropTypes from 'prop-types'

import styles from './language.module.css'

class Language extends Component {
  static contextTypes = {
    language: PropTypes.object,
  }

  state = {
    lang: '',
  }

  componentDidMount() {
    const { language } = this.context
    this.setState({
      lang: language.locale || language.detected,
    })
  }

  handleChange = lang => {
    console.log('change')
    const { language } = this.context
    const { originalPath } = language

    if (lang === this.state.lang) {
      return
    }

    this.setState({ lang }, () => {
      localStorage.setItem('language', lang)
      window.location.href = `/${lang}${originalPath}`
    })
  }

  render() {
    const { language } = this.context
    const { languages } = language
    const { lang } = this.state

    if (!lang) {
      return null
    }

    const MenuItems = languages.map(({ value, text }) => (
      <option key={value} value={value}>
        {text.slice(0, 2).toUpperCase()}
      </option>
    ))

    return (
      <div className={styles.select_container}>
        <select value={lang} onChange={e => this.handleChange(e.target.value)} className={styles.select}>
          {MenuItems}
        </select>
      </div>
    )
  }
}

export default Language
