import React from 'react'
import DrawerToggleButton from './DrawerToggleButton'
import { Link } from '../i18n'
import { Anchor } from 'grommet'
import Language from './language'
import './Toolbar.css'

import styles from './toolbar.module.css'

const toolbar = props => (
  <header className="toolbar">
    <nav className="toolbar__navigation">
      <div className="toolbar__toggle-button">
        <DrawerToggleButton click={props.drawerClickHandler} />
      </div>
      <div className="toolbar__logo">
        <div className={styles.Coral}>
          <a
            href="http://tickets.tedxuniversityofmacedonia.com"
            target="_blank"
            rel="noopener noreferrer"
          >
            <button href="#" style={{ padding: '6px 22px' }}>
              <div
                style={{
                  color: 'black',
                  display: 'inline',
                  fontSize: '1.1875rem',
                  fontWeight: '600',
                  letterSpacing: '1.9px',
                }}
              >
                Get Ticket
              </div>
              <span className={styles.Coralwave1} />
              <span className={styles.Coralwave2} />
              <span className={styles.Coralwave3} />
            </button>
          </a>
        </div>
      </div>
      <div className="spacer" />
      <div className="toolbar_navigation-items">
        <ul>
          <li>
            <Link to="/" style={{ textDecoration: 'none' }}>
              Home
            </Link>
          </li>
          <li>
            <a href="#">Event</a>
            <ul className="dropdown">
              <li>
                <Link to="/speakers" style={{ textDecoration: 'none' }}>
                  Speakers
                </Link>
              </li>
              <li>
                <Link to="/performances" style={{ textDecoration: 'none' }}>
                  Performances
                </Link>
              </li>
              <li>
                <Link to="/hosts" style={{ textDecoration: 'none' }}>
                  Hosts
                </Link>
              </li>
              <li>
                <Link to="/booklet" style={{ textDecoration: 'none' }}>
                  Booklet
                </Link>
              </li>
            </ul>
          </li>
          <li>
            <Link to="/partners" style={{ textDecoration: 'none' }}>
              Partners
            </Link>
          </li>
          <li>
            <Anchor
              href="https://blog.tedxuniversityofmacedonia.com/"
              target="_blank"
              rel="noopener noreferrer"
              style={{ textDecoration: 'none' }}
            >
              Blog
            </Anchor>
          </li>
          <li>
            <Link to="/about" style={{ textDecoration: 'none' }}>
              About
            </Link>
          </li>
          <li>
            <Language />
          </li>
        </ul>
      </div>
    </nav>
  </header>
)

export default toolbar
