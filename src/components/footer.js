import React from 'react'
import { Box, Heading } from 'grommet'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
  faFacebook,
  faInstagram,
  faLinkedin,
  faTwitter,
  faYoutube,
} from '@fortawesome/free-brands-svg-icons'
import { faHeart } from '@fortawesome/free-solid-svg-icons'

import redPattern from '../assets/red_pattern.svg'
import styles from './footer.module.css'

const SocialMedia = () => (
  <div className={styles.column_text}>
    <Heading level="3" className={styles.social_text}>
      Follow Us!
    </Heading>
    <span className={styles.social_icon}>
      <a
        href="https://www.facebook.com/TEDxUniversityofMacedonia/"
        target="_blank"
        rel="noopener noreferrer"
      >
        <FontAwesomeIcon icon={faFacebook} size="2x" />
      </a>
    </span>
    <span className={styles.social_icon}>
      <a
        href="https://www.instagram.com/tedxuniversityofmacedonia/"
        target="_blank"
        rel="noopener noreferrer"
      >
        <FontAwesomeIcon icon={faInstagram} size="2x" />
      </a>
    </span>
    <span className={styles.social_icon}>
      <a
        href="https://www.linkedin.com/company/tedxuniversityofmacedonia/"
        target="_blank"
        rel="noopener noreferrer"
      >
        <FontAwesomeIcon icon={faLinkedin} size="2x" />
      </a>
    </span>
    <span className={styles.social_icon}>
      <a
        href="https://twitter.com/TEDxUoMacedonia"
        target="_blank"
        rel="noopener noreferrer"
      >
        <FontAwesomeIcon icon={faTwitter} size="2x" />
      </a>
    </span>
    <span className={styles.social_icon}>
      <a
        href="https://www.youtube.com/channel/UCbAZ0PT5sbBgL6Ebn7vq2qg"
        target="_blank"
        rel="noopener noreferrer"
      >
        <FontAwesomeIcon icon={faYoutube} size="2x" />
      </a>
    </span>
  </div>
)

const ContactForm = () => (
  <form action="mailto:info.tedxuom@gmail.com" className={styles.form}>
    <Heading level="3" className={styles.social_text}>
      Contact Us!
    </Heading>
    <div className={styles.form_input}>
      <label htmlFor="name">Enter your name: </label>
      <input type="text" name="name" id="name" required />
    </div>
    <div className={styles.form_input}>
      <label htmlFor="email">Enter your email: </label>
      <input type="email" name="email" id="email" required />
    </div>
    <div className={styles.form_input}>
      <label htmlFor="email">Message: </label>
      <textarea name="email" id="message" required />
    </div>
    <div className={styles.form_input}>
      <input type="submit" value="Submit" />
    </div>
  </form>
)

export default () => (
  <footer style={{ fontSize: '16px' }}>
    <Box align="center">
      <div
        style={{
          width: '100%',
          backgroundImage: `url(${redPattern})`,
        }}
        className={styles.container}
      >
        <div
          style={{
            width: '100%',
            backgroundColor: 'rgba(0, 0, 0, 0.3)',
            padding: '2rem 0 4rem 0',
          }}
          className={styles.grid}
        >
          <div className={styles.column}>
            <p className={styles.column_text}>
              © TEDxUniversityofMacedonia <br />
              This Independent TEDx Event Is Operated Under License From TED
            </p>
            <p className={styles.column_text}>
              Made with <FontAwesomeIcon icon={faHeart} size="lg" /> by
              TEDxUniversityofMacedonia.
            </p>
          </div>
          <div className={styles.column}>
            <SocialMedia />
          </div>
          <div className={styles.column}>
            <ContactForm />
          </div>
        </div>
      </div>
    </Box>
  </footer>
)
