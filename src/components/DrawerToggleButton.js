import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBars } from '@fortawesome/free-solid-svg-icons'

const drawerToggleButton = props => (
  <button style={{border: 'none', zIndex: '8'}} onClick={props.click}>
    <div><FontAwesomeIcon icon={faBars}/></div>
  </button>
)
export default drawerToggleButton
