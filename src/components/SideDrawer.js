import React from 'react'
import { Link } from '../i18n'
import { Anchor } from 'grommet'
import Language from './language_mobile'
import './SideDrawer.css'

const sideDrawer = props => {
  let drawerClasses = 'side-drawer'
  console.log(props)
  if (props.show) {
    drawerClasses = 'side-drawer open'
  }
  return (
    <nav className={drawerClasses}>
      <ul>
        <li>
          <Link to="/" style={{ textDecoration: 'none' }}>
            Home
          </Link>
        </li>
        <li>
          <Link to="/speakers" style={{ textDecoration: 'none' }}>
            Speakers
          </Link>
        </li>
        <li>
          <Link to="/performances" style={{ textDecoration: 'none' }}>
            Performances
          </Link>
        </li>
        <li>
          <Link to="/hosts" style={{ textDecoration: 'none' }}>
            Hosts
          </Link>
        </li>
        <li>
          <Link to="/booklet" style={{ textDecoration: 'none' }}>
            Booklet
          </Link>
        </li>
        {/* <Anchor href="#">Performances</Anchor>
              <Anchor href="#">Workshops</Anchor> */}
        <li>
          <Link to="/partners" style={{ textDecoration: 'none' }}>
            Partners
          </Link>
        </li>
        <li>
          <Anchor
            href="https://blog.tedxuniversityofmacedonia.com/"
            target="_blank"
            rel="noopener noreferrer"
          >
            Blog
          </Anchor>
        </li>
        <li>
          <Link to="/about" style={{ textDecoration: 'none' }}>
            About
          </Link>
        </li>
        <Language />
      </ul>
    </nav>
  )
}

export default sideDrawer
