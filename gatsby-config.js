const autoprefixer = require('autoprefixer')

module.exports = {
  siteMetadata: {
    title: 'TEDxUniversityofMacedonia 2018 Chasm',
  },
  pathPrefix: '/gatsby-starter-grommet',
  plugins: [
    { resolve: `gatsby-plugin-sass` },
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        trackingId: 'UA-62018568-1',
        // Puts tracking script in the head instead of the body
        head: false,
        // Setting this parameter is optional
        anonymize: true,
        // Setting this parameter is also optional
        respectDNT: true,
        // Avoids sending pageview hits from custom paths
        exclude: ['/preview/**', '/do-not-track/me/too/'],
      },
    },
    `gatsby-plugin-styled-components`
  ],
}
